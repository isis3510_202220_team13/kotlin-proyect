package com.example.nutri_connected.activities.friends.di

import android.app.Application
import androidx.room.Room
import com.example.nutri_connected.activities.friends.api.FriendListAPI
import com.example.nutri_connected.activities.friends.data.FriendListDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(FriendListAPI.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideCarListAPI(retrofit: Retrofit): FriendListAPI =
        retrofit.create(FriendListAPI::class.java)

    @Provides
    @Singleton
    fun provideDatabase(app: Application): FriendListDatabase =
        Room.databaseBuilder(app, FriendListDatabase::class.java, "carlist_database")
            .build()
}