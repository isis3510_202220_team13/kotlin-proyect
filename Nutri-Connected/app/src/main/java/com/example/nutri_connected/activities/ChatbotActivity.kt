package com.example.nutri_connected.activities

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.example.nutri_connected.R

class ChatbotActivity : BaseActivity() {
    /**
     * Reference to the toolbar
     */
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends_list)

        // Initialize attributes
//        this.toolbar = findViewById<Toolbar>(R.id.toolbar_chatbot_activity)

        setUpActionBar()
    }

    /**
     * Sets up the action bar of the application
     */
    private fun setUpActionBar(){
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
            actionBar.title = resources.getString(R.string.chatbot)
        }

        toolbar.setNavigationOnClickListener{
            onBackPressed()
        }
    }
}