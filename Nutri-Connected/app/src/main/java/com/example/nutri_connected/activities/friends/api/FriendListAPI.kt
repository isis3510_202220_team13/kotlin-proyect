package com.example.nutri_connected.activities.friends.api

import com.example.nutri_connected.activities.friends.data.FriendList
import retrofit2.http.GET

interface FriendListAPI {
    companion object{
        const val BASE_URL = "https://random-data-api.com/api/v2/"
    }

    // The number of cars can be varied using the size.
    // By default it is kept at 20, but can be tweaked.
    @GET("users?size=20&is_xml=true")
    suspend fun getFriendList() : List<FriendList>

}