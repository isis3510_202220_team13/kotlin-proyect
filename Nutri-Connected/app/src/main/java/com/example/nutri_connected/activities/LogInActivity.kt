package com.example.nutri_connected.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.nutri_connected.Constants
import com.example.nutri_connected.R
import com.example.nutri_connected.models.User
import com.google.firebase.auth.*
import java.util.regex.Pattern

class LogInActivity : BaseActivity() {
    /**
     * Firebase authentication service instance
     */
    private lateinit var auth : FirebaseAuth

    /**
     * Reference to the input of email
     */
    private lateinit var inputEmail: EditText

    /**
     * Reference to the input of password
     */
    private lateinit var inputPassword: EditText

    /**
     * Reference to the sign in link
     */
    private lateinit var registrationLink: TextView

    /**
     * Reference to the sign in link
     */
    private lateinit var buttonSignIn: Button

    /**
     * Constructor function
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        // Initialize attributes
        this.auth = FirebaseAuth.getInstance()
        this.inputEmail = findViewById<EditText>(R.id.input_email_login)
        this.inputPassword = findViewById<EditText>(R.id.input_password_login)
        this.buttonSignIn = findViewById<Button>(R.id.button_signIn)
        this.registrationLink = findViewById<TextView>(R.id.text_signUp)

        // Initialize listener for the register button
        this.buttonSignIn.setOnClickListener{
            signInRegisteredUser()
        }

        // Initialize click listener for registration link
        this.registrationLink.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }
    }

    /**
     * A function for Sign-In using the registered user using the email and password.
     */
    private fun signInRegisteredUser() {
        // Here we get the text from editText and trim the space
        val email: String = this.inputEmail.text.toString().trim { it <= ' ' }
        val password: String = this.inputPassword.text.toString().trim { it <= ' ' }

        if(!Constants.isNetworkAvailable(this)){
            Toast.makeText(this@LogInActivity, "No internet connection", Toast.LENGTH_SHORT).show()
        } else if (validateForm(email, password)) {
            // Show the progress dialog.
            showProgressDialog()

            // Sign-In using FirebaseAuth
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    hideProgressDialog()
                    if (task.isSuccessful) {
                        Toast.makeText(
                            this@LogInActivity,
                            "You have successfully signed in.",
                            Toast.LENGTH_LONG
                        ).show()

                        startActivity(Intent(this@LogInActivity, MainActivity::class.java))
                    } else {
                        Log.e("EXCEPTION", task.exception.toString())
                        try {
                            throw task.exception!!
                        } catch (e: FirebaseAuthInvalidCredentialsException) {
                            if(e.message!!.startsWith("The password")){
                                Toast.makeText(
                                    this@LogInActivity,
                                    "The password is not valid",
                                    Toast.LENGTH_LONG
                                ).show()
                            } else if(e.message!!.startsWith("There is no user")){
                                Toast.makeText(
                                    this@LogInActivity,
                                    "The email is not registered",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        } catch (e: Exception) {
                            Log.e("UNHANDLED-AUTH-EXCEP", e.toString())

                            Toast.makeText(
                                this@LogInActivity,
                                "Sign in failed",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
        }
    }

    /**
     * A function to validate the entries of a user.
     */
    private fun validateForm(email: String, password: String): Boolean {
        val emailAddressPattern = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )

        return when {
            TextUtils.isEmpty(email) -> {
                showErrorSnackBar("Please enter email.")
                false
            }
            TextUtils.isEmpty(email) -> {
                showErrorSnackBar("Please enter email.")
                false
            }
            !emailAddressPattern.matcher(email).matches() -> {
                showErrorSnackBar("Please provide a valid email.")
                false
            }
            TextUtils.isEmpty(password) -> {
                showErrorSnackBar("Please enter password.")
                false
            }
            else -> {
                true
            }
        }
    }

    fun signInSuccess(user : User){
        hideProgressDialog()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}