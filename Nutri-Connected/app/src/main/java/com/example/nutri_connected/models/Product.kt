package com.example.nutri_connected.models

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

data class Product(
    @Expose var code : String,
    @Expose var product: ProductDetails,
    @Expose var status : Int
)
