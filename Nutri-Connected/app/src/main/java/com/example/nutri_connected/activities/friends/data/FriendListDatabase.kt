package com.example.nutri_connected.activities.friends.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.abhishek.carlist.data.FriendsDao

@Database(entities = [FriendList::class], version = 1)
abstract class FriendListDatabase : RoomDatabase() {
    abstract fun friendsDao(): FriendsDao
}
