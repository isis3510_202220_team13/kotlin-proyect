package com.example.nutri_connected.activities.friends.data

import androidx.room.Entity
import androidx.room.PrimaryKey

// Data Class to store the data
@Entity(tableName = "users")
data class FriendList(
    @PrimaryKey val id: String,
    val first_name: String,
    val last_name: String,
    val username: String,
    val email: String,
    val avatar: String
)