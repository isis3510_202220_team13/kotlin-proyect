package com.example.nutri_connected.activities.friends.carlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.nutri_connected.activities.friends.data.FriendList
import com.example.nutri_connected.databinding.FriendlistItemBinding



class FriendAdapter : ListAdapter<FriendList, FriendAdapter.FriendViewHolder>(FriendListComparator()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendViewHolder {
        val binding =
            FriendlistItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FriendViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FriendViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

    class FriendViewHolder(private val binding: FriendlistItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(friendlist: FriendList) {
            binding.apply {
                carName.text = friendlist.id
                carTransmission.text = friendlist.first_name
                carColor.text = friendlist.last_name
                carDriveType.text = friendlist.username
                carFuelType.text = friendlist.email
                ivFriendImage
                Glide.with(ivFriendImage).load(friendlist.avatar).into(ivFriendImage);


            }
        }
    }

    class FriendListComparator : DiffUtil.ItemCallback<FriendList>() {
        override fun areItemsTheSame(oldItem: FriendList, newItem: FriendList) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: FriendList, newItem: FriendList) =
            oldItem == newItem
    }
}
