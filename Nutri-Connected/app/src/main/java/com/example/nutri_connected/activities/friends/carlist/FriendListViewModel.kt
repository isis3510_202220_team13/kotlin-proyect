package com.example.nutri_connected.activities.friends.carlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.example.nutri_connected.activities.friends.data.FriendListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FriendListViewModel @Inject constructor(
    repository: FriendListRepository
) : ViewModel() {
    val friends = repository.getFriends().asLiveData()
}