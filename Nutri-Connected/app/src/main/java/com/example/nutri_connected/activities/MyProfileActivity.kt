package com.example.nutri_connected.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.webkit.MimeTypeMap
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.example.nutri_connected.Constants
import com.example.nutri_connected.R
import com.example.nutri_connected.firebase.FirestoreClass
import com.example.nutri_connected.models.User
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import de.hdodenhof.circleimageview.CircleImageView
import java.io.IOException

class MyProfileActivity : BaseActivity() {
    companion object {
        private const val READ_STORAGE_PERMISSION_CODE = 1
        private const val PICK_IMAGE_REQUEST_CODE = 2
    }

    private var mSelectedImageFileUri: Uri? = null
    private var mProfileImageURL : String = ""
    private lateinit var mUserDetails: User

    /**
     * Reference to the toolbar
     */
    private lateinit var toolbarMyProfile: Toolbar

    /**
     * Reference to the image
     */
    private lateinit var ivUserImage: CircleImageView

    /**
     * Reference to the the name edit text
     */
    private lateinit var etName: AppCompatEditText

    /**
     * Reference to the the email edit text
     */
    private lateinit var etEmail: AppCompatEditText

    /**
     * Reference to the vegan checkbox
     */
    private lateinit var cbVegan: CheckBox

    /**
     * Reference to the vegan checkbox
     */
    private lateinit var cbVeggie: CheckBox

    /**
     * Reference to the vegan checkbox
     */
    private lateinit var cbPaleo: CheckBox

    /**
     * Reference to the vegan checkbox
     */
    private lateinit var cbKeto: CheckBox

    /**
     * Reference to the vegan checkbox
     */
    private lateinit var btnUpdate: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)

        this.ivUserImage = findViewById<CircleImageView>(R.id.iv_user_image)
        this.etName = findViewById<AppCompatEditText>(R.id.et_name)
        this.etEmail = findViewById<AppCompatEditText>(R.id.et_email)
        this.cbVegan = findViewById<CheckBox>(R.id.cb_vegan)
        this.cbVeggie = findViewById<CheckBox>(R.id.cb_veggie)
        this.cbPaleo = findViewById<CheckBox>(R.id.cb_paleo)
        this.cbKeto = findViewById<CheckBox>(R.id.cb_keto)
        this.btnUpdate = findViewById<Button>(R.id.btn_update)


        setupActionBar()

        FirestoreClass().loadUserData(this)

        ivUserImage.setOnClickListener{
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED){
                showImageChooser()
            } else{
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    READ_STORAGE_PERMISSION_CODE
                )
            }
        }

        btnUpdate.setOnClickListener {
            if(mSelectedImageFileUri != null){
                uploadUserImage()
            } else{
                showProgressDialog()

                updateUserProfileData()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == READ_STORAGE_PERMISSION_CODE){
            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                showImageChooser()
            } else{
                Toast.makeText(
                    this,
                    "Oops, you just denied permission for storage.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun showImageChooser(){
        var galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE_REQUEST_CODE && data!!.data != null){
            mSelectedImageFileUri = data.data

            try{
                Glide
                    .with(this)
                    .load(mSelectedImageFileUri)
                    .centerCrop()
                    .placeholder(R.drawable.ic_user_place_holder)
                    .into(ivUserImage)
            }catch (e: IOException){
                e.printStackTrace()
            }
        }
    }

    /**
     * A function to setup action bar
     */
    private fun setupActionBar() {

        this.toolbarMyProfile = findViewById<Toolbar>(R.id.toolbar_my_profile_activity)

        setSupportActionBar(toolbarMyProfile)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
            actionBar.title = resources.getString(R.string.my_profile)
        }

        toolbarMyProfile.setNavigationOnClickListener { onBackPressed() }
    }

    fun setUserDataInUi(user: User){

        mUserDetails = user

        Glide
            .with(this)
            .load(user.image)
            .centerCrop()
            .placeholder(R.drawable.ic_user_place_holder)
            .into(ivUserImage)

        etName.setText(user.name)
        etEmail.setText(user.email)

        var found = user.diets.findLast { item -> item == "VEGAN" }
        if(found != null){cbVegan.isChecked=true}
        found = user.diets.findLast { item -> item == "VEGGIE" }
        if(found != null){cbVeggie.isChecked=true}
        found = user.diets.findLast { item -> item == "PALEO" }
        if(found != null){cbPaleo.isChecked=true}
        found = user.diets.findLast { item -> item == "KETO" }
        if(found != null){cbKeto.isChecked=true}
    }

    private fun updateUserProfileData(){
        val userHashMap = HashMap<String, Any>()

        var anyChangesMade = false
        var dietChanged = false

        if(mProfileImageURL.isNotEmpty() && mProfileImageURL != mUserDetails.image){
            userHashMap[Constants.IMAGE] = mProfileImageURL
            anyChangesMade = true
        }

        if(etName.text.toString() != mUserDetails.name){
            userHashMap[Constants.NAME] = etName.text.toString()
            anyChangesMade = true
        }

        var found = mUserDetails.diets.findLast { item -> item == "VEGAN" }
        if((found == null && cbVegan.isChecked) || (found != null && !cbVegan.isChecked)){dietChanged = true}
        found = mUserDetails.diets.findLast { item -> item == "VEGGIE" }
        if((found == null && cbVeggie.isChecked) || (found != null && !cbVeggie.isChecked)){dietChanged = true}
        found = mUserDetails.diets.findLast { item -> item == "PALEO" }
        if((found == null && cbPaleo.isChecked) || (found != null && !cbPaleo.isChecked)){dietChanged = true}
        found = mUserDetails.diets.findLast { item -> item == "KETO" }
        if((found == null && cbKeto.isChecked) || (found != null && !cbKeto.isChecked)){dietChanged = true}

        if(dietChanged){
            var newDiet : List<String> = emptyList()
            if(cbVegan.isChecked){newDiet += "VEGAN"}
            if(cbVeggie.isChecked){newDiet += "VEGGIE"}
            if(cbPaleo.isChecked){newDiet += "PALEO"}
            if(cbKeto.isChecked){newDiet += "KETO"}

            userHashMap[Constants.DIETS] = newDiet

            anyChangesMade = true
        }
        if(anyChangesMade){
            FirestoreClass().updateUserProfileData(this,userHashMap)
        }
    }

    private fun uploadUserImage(){
        showProgressDialog()

        if(mSelectedImageFileUri != null){
            val sRef : StorageReference = FirebaseStorage.getInstance().reference.child(
                "User_IMAGE"
                    + System.currentTimeMillis()
                    + "." + getFileExtension(mSelectedImageFileUri))

            sRef.putFile(mSelectedImageFileUri!!).addOnSuccessListener {
                taskSnapshot ->
                Log.i("Firebase Image URL",
                taskSnapshot.metadata!!.reference!!.downloadUrl.toString()
                )

                taskSnapshot.metadata!!.reference!!.downloadUrl.addOnSuccessListener {
                    uri ->
                    Log.i("Downloadable Iamge URL", uri.toString())
                    mProfileImageURL = uri.toString()

                    updateUserProfileData()
                }
            }.addOnFailureListener{
                exception ->
                Toast.makeText(
                    this@MyProfileActivity,
                    exception.message,
                    Toast.LENGTH_LONG
                ).show()

                hideProgressDialog()
            }
        }
    }

    private fun getFileExtension(uri: Uri?): String?{
        return MimeTypeMap.getSingleton().getExtensionFromMimeType(contentResolver.getType(uri!!))
    }

    fun profileUpdateSuccess(){
        hideProgressDialog()

        setResult(Activity.RESULT_OK)

        finish()
    }
}