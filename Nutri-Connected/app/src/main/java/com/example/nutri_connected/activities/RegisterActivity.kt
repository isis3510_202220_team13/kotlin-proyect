package com.example.nutri_connected.activities

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.nutri_connected.Constants
import com.example.nutri_connected.R
import com.example.nutri_connected.firebase.FirestoreClass
import com.example.nutri_connected.models.User
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.*
import java.util.regex.Pattern

class RegisterActivity : BaseActivity() {
    /**
     * Reference to the input of name
     */
    private lateinit var inputName: EditText

    /**
     * Reference to the input of email
     */
    private lateinit var inputEmail: EditText

    /**
     * Reference to the input of password
     */
    private lateinit var inputPassword: EditText

    /**
     * Reference to the input of password confirmation
     */
    private lateinit var inputConfirmPassword: EditText

    /**
     * Reference to the sign in link
     */
    private lateinit var signInLink: TextView

    /**
     * Reference to the sign in link
     */
    private lateinit var buttonRegister: Button

    /**
     * Constructor function
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // Initialize attributes
        this.inputName = findViewById<EditText>(R.id.input_name)
        this.inputEmail = findViewById<EditText>(R.id.input_email)
        this.inputPassword = findViewById<EditText>(R.id.input_password)
        this.inputConfirmPassword = findViewById<EditText>(R.id.input_password_confirm)
        this.buttonRegister = findViewById<Button>(R.id.button_register)
        this.signInLink = findViewById<TextView>(R.id.text_singIn)

        // Initialize listener for the register button
        this.buttonRegister.setOnClickListener{
            registerUser()
        }

        // Initialize listener for sign in link
        this.signInLink.setOnClickListener {
            startActivity(Intent(this, LogInActivity::class.java))
            finish()
        }
    }

    /**
     * A function to register a user to our app using the Firebase.
     * For more details visit: https://firebase.google.com/docs/auth/android/custom-auth
     */
    private fun registerUser() {
        val nameText: String = this.inputName.text.toString()
        val emailText: String = this.inputEmail.text.toString().trim { it <= ' ' }
        val passwordText: String = this.inputPassword.text.toString().trim { it <= ' ' }
        val confirmPasswordText: String = this.inputConfirmPassword.text.toString().trim { it <= ' ' }

        if(!Constants.isNetworkAvailable(this)){
            Toast.makeText(this@RegisterActivity, "No internet connection", Toast.LENGTH_SHORT).show()
        }else if (validateForm(nameText, emailText, passwordText, confirmPasswordText)) {
            // Show the progress dialog.
            showProgressDialog()
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailText, passwordText)
                .addOnCompleteListener(
                    OnCompleteListener<AuthResult> { task ->

                        // If the registration is successfully done
                        if (task.isSuccessful) {

                            // Firebase registered user
                            val firebaseUser: FirebaseUser = task.result!!.user!!
                            // Registered Email
                            val registeredEmail = firebaseUser.email!!

                            val user = User(firebaseUser.uid, nameText, registeredEmail)
                            FirestoreClass().registerUser(this, user)
                        } else {
                            Log.e("EXCEPTION", task.exception.toString())
                            try {
                                throw task.exception!!
                            } catch (e: FirebaseAuthUserCollisionException) {
                                Toast.makeText(
                                    this@RegisterActivity,
                                    "The email is already registered",
                                    Toast.LENGTH_LONG
                                ).show()
                            } catch (e: FirebaseAuthWeakPasswordException) {
                                Toast.makeText(
                                    this@RegisterActivity,
                                    "The email is already registered",
                                    Toast.LENGTH_LONG
                                ).show()
                            } catch (e: Exception) {
                                Log.e("UNHANDLED-AUTH-EXCEP", e.toString())

                                Toast.makeText(
                                    this@RegisterActivity,
                                    "Registration failed",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    })
        }
    }

    /**
     * A function to validate the entries of a new user.
     */
    private fun validateForm(name: String, email: String, password: String, confirmPassword: String): Boolean {
        val emailAddressPattern = Pattern.compile(
    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"
        )

        return when {
            TextUtils.isEmpty(name) -> {
                showErrorSnackBar("Please enter name.")
                false
            }
            TextUtils.isEmpty(email) -> {
                showErrorSnackBar("Please enter email.")
                false
            }
            TextUtils.isEmpty(email) -> {
                showErrorSnackBar("Please enter email.")
                false
            }
            !emailAddressPattern.matcher(email).matches() -> {
                showErrorSnackBar("Please provide a valid email.")
                false
            }
            TextUtils.isEmpty(password) -> {
                showErrorSnackBar("Please enter password.")
                false
            }
            TextUtils.isEmpty(confirmPassword) -> {
                showErrorSnackBar("Please confirm password.")
                false
            }
            confirmPassword != password -> {
                showErrorSnackBar("The passwords do not match.")
                false
            }
            else -> {
                true
            }
        }
    }

    fun userRegisteredSuccess(){
        Toast.makeText(
            this@RegisterActivity,
            "You have successfully registered",
            Toast.LENGTH_SHORT
        ).show()

        hideProgressDialog()

        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, LogInActivity::class.java))
        finish()
    }
}