package com.example.nutri_connected.services

import com.example.nutri_connected.models.Product
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface ProductService {
    @GET
    suspend fun getProductByBarCode(@Url url:String): Response<Product>
}