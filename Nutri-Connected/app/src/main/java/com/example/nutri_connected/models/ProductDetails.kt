package com.example.nutri_connected.models

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

data class ProductDetails(
    @Expose var product_name : String,
)
