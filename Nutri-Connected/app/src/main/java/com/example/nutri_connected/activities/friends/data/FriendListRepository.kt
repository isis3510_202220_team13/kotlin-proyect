package com.example.nutri_connected.activities.friends.data

import androidx.room.withTransaction
import com.example.nutri_connected.activities.friends.api.FriendListAPI
import com.abhishek.carlist.util.networkBoundResource
import kotlinx.coroutines.delay
import javax.inject.Inject

class FriendListRepository @Inject constructor(
    private val api: FriendListAPI,
    private val db: FriendListDatabase
) {
    private val friendsDao = db.friendsDao()

    fun getFriends() = networkBoundResource(
        query = {
            friendsDao.getAllCars()
        },
        fetch = {
            delay(2000)
            api.getFriendList()
        },
        saveFetchResult = { CarList ->
            db.withTransaction {
                friendsDao.deleteAllFriends()
                friendsDao.insertFriends(CarList)
            }
        }
    )
}