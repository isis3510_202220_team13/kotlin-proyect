package com.example.nutri_connected.activities.List

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nutri_connected.databinding.ListMainBinding
import androidx.appcompat.widget.Toolbar
import com.example.nutri_connected.R

class ListActivity : AppCompatActivity(), TaskItemClickListener
{
    private lateinit var binding: ListMainBinding
    private lateinit var taskViewModel: TaskViewModel
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ListMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        this.toolbar = findViewById<Toolbar>(R.id.toolbar_list_activity)
        taskViewModel = ViewModelProvider(this)[TaskViewModel::class.java]
        binding.newTaskButton.setOnClickListener {
            NewTaskSheet(null).show(supportFragmentManager, "newTaskTag")
        }
        setUpActionBar()
        setRecyclerView()
    }

    private fun setRecyclerView()
    {
        val listActivity = this
        taskViewModel.taskItems.observe(this){
            binding.todoListRecyclerView.apply {
                layoutManager = LinearLayoutManager(applicationContext)
                adapter = TaskItemAdapter(it!!, listActivity)
            }
        }
    }

    override fun editTaskItem(taskItem: TaskItem)
    {
        NewTaskSheet(taskItem).show(supportFragmentManager,"newTaskTag")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun completeTaskItem(taskItem: TaskItem)
    {
        taskViewModel.setCompleted(taskItem)
    }
    override fun deleteTaskItem(taskItem: TaskItem)
    {
        taskViewModel.deleteTaskItem(taskItem)
    }
    private fun setUpActionBar(){
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
            actionBar.title = resources.getString(R.string.app_name)
        }

        toolbar.setNavigationOnClickListener{
            onBackPressed()
        }
    }
}







