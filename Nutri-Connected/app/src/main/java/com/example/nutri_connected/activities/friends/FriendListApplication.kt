package com.example.nutri_connected.activities.friends

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FriendListApplication : Application()