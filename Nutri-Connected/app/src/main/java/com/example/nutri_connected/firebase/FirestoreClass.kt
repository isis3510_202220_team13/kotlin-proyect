package com.example.nutri_connected.firebase

import android.app.Activity
import android.util.Log
import android.widget.Toast
import com.example.nutri_connected.Constants
import com.example.nutri_connected.activities.LogInActivity
import com.example.nutri_connected.activities.MainActivity
import com.example.nutri_connected.activities.MyProfileActivity
import com.example.nutri_connected.activities.RegisterActivity
import com.example.nutri_connected.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions

class FirestoreClass {

    private val mFireStore = FirebaseFirestore.getInstance()

    fun registerUser(activity: RegisterActivity, userInfo : User){
        mFireStore.collection(Constants.USERS)
            .document(getCurrentUserEmail())
            .set(userInfo, SetOptions.merge())
            .addOnSuccessListener {
                activity.userRegisteredSuccess()
            }.addOnFailureListener{
                e ->
                Log.e(activity.javaClass.simpleName, "Error registering user")
            }
    }

    fun updateUserProfileData(activity : MyProfileActivity, userHashMap: HashMap<String, Any>){
        mFireStore.collection(Constants.USERS)
            .document(getCurrentUserEmail())
            .update(userHashMap)
            .addOnSuccessListener {
                Log.i(activity.javaClass.simpleName, "Profile data updated succesfully")
                Toast.makeText(activity, "Profile updated successfully!", Toast.LENGTH_SHORT)
                activity.profileUpdateSuccess()
            }.addOnFailureListener{
                e ->
                activity.hideProgressDialog()
                Log.e(activity.javaClass.simpleName, "Error while updating profile")
                Toast.makeText(activity, "Wrror updating profile!", Toast.LENGTH_SHORT)
            }
    }

    fun loadUserData(activity: Activity){
        mFireStore.collection(Constants.USERS)
            .document(getCurrentUserEmail())
            .get()
            .addOnSuccessListener { document ->
                val loggedInUser = document.toObject(User::class.java)!!
                when(activity){
                    is LogInActivity -> {
                        activity.signInSuccess(loggedInUser)
                    }
                    is MainActivity -> {
                        activity.updateNavigationUserDetails(loggedInUser)
                    }
                    is MyProfileActivity -> {
                        activity.setUserDataInUi(loggedInUser)
                    }
                }
            }.addOnFailureListener{
                    e ->
                when(activity){
                    is LogInActivity -> {
                        activity.hideProgressDialog()
                    }
                    is MainActivity -> {
                        activity.hideProgressDialog()
                    }
                }
                Log.e(activity.javaClass.simpleName, "Error registering user")
            }
    }

    fun getCurrentUserId(): String{
        var currentUser = FirebaseAuth.getInstance().currentUser
        var currentUserID = ""
        if (currentUser != null){
            currentUserID = currentUser.uid
        }

        return currentUserID
    }

    fun getCurrentUserEmail(): String{
        var currentUser = FirebaseAuth.getInstance().currentUser
        var currentEmail = ""
        if (currentUser != null){
            currentEmail = currentUser.email!!
        }

        return currentEmail
    }
}