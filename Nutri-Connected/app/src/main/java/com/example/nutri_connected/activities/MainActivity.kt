package com.example.nutri_connected.activities

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.LruCache
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.bumptech.glide.Glide
import com.example.nutri_connected.Constants
import com.example.nutri_connected.R
import com.example.nutri_connected.activities.List.ListActivity
import com.example.nutri_connected.activities.friends.carlist.FriendActivity
import com.example.nutri_connected.databinding.ActivityMainBinding
import com.example.nutri_connected.firebase.FirestoreClass
import com.example.nutri_connected.models.Product
import com.example.nutri_connected.models.User
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import com.google.gson.GsonBuilder
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.GlobalScope


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object{
        const val MY_PROFILE_REQUEST_CODE : Int = 11
    }

    /**
     * LRU cache storing recently scanned products
     */
    private lateinit var memoryCache: LruCache<String, Product>

    private lateinit var binding : ActivityMainBinding

    /**
     * Constructor function
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initialize LRU cache
        val maxMemory =(Runtime.getRuntime().maxMemory()/1024).toInt()
        val cacheSize = maxMemory / 100
        memoryCache = object : LruCache<String, Product>(cacheSize){}

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Set up actions
        setUpActionBar()
        binding.appBarMain.mainContent.buttonBarcodeScan.setOnClickListener{ initScanner() }
        binding.appBarMain.mainContent.buttonProfile.setOnClickListener{ goToMyProfileView() }
        binding.appBarMain.mainContent.buttonFriends.setOnClickListener{ goToMyFriendsView() }

        // Set up navigation action
        binding.navView.setNavigationItemSelectedListener(this)

        FirestoreClass().loadUserData(this)
    }

    override fun onResume() {
        super.onResume()

        FirestoreClass().loadUserData(this)
    }

    /**
     * Sets up the action bar of the application
     */
    private fun setUpActionBar(){

        setSupportActionBar(binding.appBarMain.toolbarMainActivity)
        binding.appBarMain.toolbarMainActivity.setNavigationIcon(R.drawable.ic_action_navigation_menu)
        binding.appBarMain.toolbarMainActivity.setNavigationOnClickListener{
            toggleDrawer()
        }
    }

    /**
     * Toggles the drawer
     */
    private fun toggleDrawer(){
        if(binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else{
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    /**
     * Overrides the function of the back button being pressed
     */
    override fun onBackPressed() {
        if(binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else{
            doubleBackToExit()
        }
    }

    /**
     * Initializes the barcode scanner
     */
    private fun initScanner() {
        val integrator = IntentIntegrator(this)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.PRODUCT_CODE_TYPES)
        integrator.setPrompt("Scan a barcode")
        integrator.setBeepEnabled(true)
        integrator.initiateScan()
    }

    /**
     * Redirects the user to my profile view
     */
    private fun goToMyProfileView(){
        startActivity(Intent(this, MyProfileActivity::class.java))
    }

    /**
     * Redirects the user to my friends view
     */
    private fun goToMyFriendsView(){
        startActivity(Intent(this, FriendActivity::class.java))
    }

    /**
     * Manages the result of the activity of the scanner
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        // Checks if there was no errors with the barcode scan
        if(result != null) {
            // Checks if the user canceled the barcode scan
            if(result.contents == null){
                Toast.makeText(this@MainActivity, "Canceled barcode scan", Toast.LENGTH_SHORT).show()
            } else {
                var productInCache = memoryCache.get("${result.contents}")
                // Network first approach to eventual connectivity issues
                if(productInCache == null){
                    if(!Constants.isNetworkAvailable(this)){
                        Toast.makeText(this@MainActivity, "No internet connection", Toast.LENGTH_SHORT).show()
                    } else {
                        var task = openFoodFactsApiCall("${result.contents}")

                        GlobalScope.launch(context = Dispatchers.Main){
                            task.execute()
                        }

                        GlobalScope.launch(context = Dispatchers.Main){
                            val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
                            val responseData = gson.fromJson(task?.get().toString(), Product::class.java)
                            if(responseData.status == 0){
                                Toast.makeText(this@MainActivity, "Product with barcode: ${responseData.code} was not found", Toast.LENGTH_LONG).show()
                            } else {
                                Toast.makeText(this@MainActivity, "${responseData.product.product_name}", Toast.LENGTH_LONG).show()
                                memoryCache.put("${result.contents}", responseData)
                            }
                        }
                    }
                } else {
                    Toast.makeText(this@MainActivity, "${productInCache.product.product_name}", Toast.LENGTH_LONG).show()
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    /**
     * Gets the result from the call to Open Food Facts API
     */
    private inner class openFoodFactsApiCall(private val barcode : String): AsyncTask<Any, Void, String>(){

        override fun onPreExecute() {
            super.onPreExecute()
            showProgressDialog()
        }

        override fun doInBackground(vararg p0: Any?): String {
            var result: String

            var connection: HttpURLConnection? = null

            try{
                val url = URL("https://world.openfoodfacts.org/api/v0/product/$barcode.json")
                connection = url.openConnection() as HttpURLConnection
                connection.doInput = true
                connection.doOutput = true

                val httpResult : Int = connection.responseCode
                if(httpResult == HttpURLConnection.HTTP_OK){
                    val inputStream = connection.inputStream

                    val reader = BufferedReader(InputStreamReader(inputStream))
                    val stringBuilder = StringBuilder()
                    var line: String?
                    try{
                        while(reader.readLine().also{line = it} != null){
                            stringBuilder.append(line + "\n")
                        }
                    } catch(e: IOException){
                        e.printStackTrace()
                    }
                    result = stringBuilder.toString()
                } else {
                    result = connection.responseMessage
                }
            } catch (e: Exception){
                result = "Exception"
            } finally {
                connection?.disconnect()
            }

            return result
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            hideProgressDialog()
        }
    }

    /**
     * Overrides the functionality of the navigarion items selected
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_home -> {
                // TODO: Set up navigation to home
            }
            R.id.nav_sign_out -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, LogInActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)

        return true
    }

    fun updateNavigationUserDetails(user: User){
        Glide
            .with(this)
            .load(user.image)
            .centerCrop()
            .placeholder(R.drawable.ic_user_place_holder)
            .into(binding.navView.getHeaderView(0).findViewById<CircleImageView>(R.id.nav_user_image))

        binding.navView.getHeaderView(0).findViewById<TextView>(R.id.tv_username).text = user.name
    }
}